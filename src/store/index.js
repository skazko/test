import Vue from "vue";
import Vuex from "vuex";
import * as modules from "./modules";
import * as http from "@/api/http";
import { START_LOADING, STOP_LOADING } from "./mutations/common";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules,
});

function requestHandler(config) {
  store.commit(`common/${START_LOADING}`);
  return config;
}

function basicResponseHandler({ data }) {
  store.commit(`common/${STOP_LOADING}`);

  if (data) {
    return data;
  }

  store.dispatch("common/addError", "Неожиданный ответ сервера");
}

function errorHandler(e) {
  store.commit(`common/${STOP_LOADING}`);
  if (e.response) {
    store.dispatch("common/addError", "Ошибка " + e.response.status);
  } else if (e.request) {
    store.dispatch("common/addError", "Неудалось получить ответ с сервера");
  } else {
    store.dispatch("common/addError", "Что-то пошло не так");
  }
  console.error(e);
}

http.addRequestHandler(requestHandler);
http.addErrorHandler(errorHandler);
http.addResponseHandler(basicResponseHandler);

export default store;
