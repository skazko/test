import * as commonMutations from "@/store/mutations/common";

const initState = () => ({
  errors: [],
  loading: 0,
});

export const common = {
  namespaced: true,
  state: initState,
  getters: {
    errors: (state) => state.errors,
    loading: (state) => !!state.loading,
  },
  mutations: {
    [commonMutations.ADD_ERROR]: (state, error) => state.errors.push(error),
    [commonMutations.REMOVE_ERROR]: (state, errorId) => {
      const { errors } = state;
      const errorIdx = errors.findIndex(({ id }) => id === errorId);
      if (errorIdx !== -1) {
        state.errors.splice(errorIdx, 1);
      }
    },
    [commonMutations.START_LOADING]: (state) => (state.loading += 1),
    [commonMutations.STOP_LOADING]: (state) => (state.loading -= 1),
  },
  actions: {
    addError({ commit, state: { errors } }, error) {
      const idx = errors.length;
      commit(commonMutations.ADD_ERROR, { error, id: idx });
      setTimeout(() => {
        commit(commonMutations.REMOVE_ERROR, idx);
      }, 3000);
    },
  },
};
