import { difference } from "lodash";
import * as holidaysApi from "@/api/holidaysApi";
import * as holidaysMutations from "@/store/mutations/holidays";

const initState = () => ({
  dates: [],
});

export const holidays = {
  namespaced: true,
  state: initState,
  getters: {
    holidays: (state) => state.dates,
  },

  mutations: {
    [holidaysMutations.SET]: (state, dates) => (state.dates = dates),
  },
  actions: {
    async getHolidays({ commit }) {
      const response = await holidaysApi.get();
      commit(holidaysMutations.SET, response);
      return response;
    },
    async saveHolidays({ commit, state: { dates } }, newDates) {
      const payload = [
        ...newDates.map((date) => ({ date, value: true })),
        ...difference(dates, newDates).map((date) => ({ date, value: false })),
      ];
      const response = await holidaysApi.save(payload);
      commit(holidaysMutations.SET, response);
      return response;
    },
  },
};
