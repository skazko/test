import http from "@/api/http";

export async function get() {
  let res = await http.get("/");
  return res;
}

export async function save(data) {
  let res = await http.post("/save/", data);
  return res;
}
